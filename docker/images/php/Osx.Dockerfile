#+++++++++++++++++++++++++++++++++++++++
# Dockerfile for teufels/docker_php_fpm
#+++++++++++++++++++++++++++++++++++++++

#FROM teufels/docker_php_fpm:7.1
FROM teufels/docker_php_fpm:7.2

# link xdebug config for osx
#RUN ln -sf /opt/docker/etc/php/xdebugfordockerformac.ini /usr/local/etc/php/conf.d/xdebugfordockerformac.ini